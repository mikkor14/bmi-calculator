﻿using System;

namespace BmiCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your weight in kilograms");
            double weight = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Please enter your height in centimeters");
            double height = Convert.ToDouble(Console.ReadLine());

            CheckBMI(CalcBMI(weight, height));
        }

        static double CalcBMI(double w, double h)
        {
            double bmi = w / Math.Pow(h / 100.0, 2);

            Console.WriteLine($"Your BMI is {bmi}");

            return bmi;
        }

        static void CheckBMI(double bmi)
        {
            if(bmi < 18.5)
            {
                Console.WriteLine("You are underweight");

            }
            else if (bmi >= 18.5 && bmi <= 24.9)
            {
                Console.WriteLine("You are healthy weight");

            }
            else if (bmi >= 25 && bmi <= 29.9)
            {
                Console.WriteLine("You are overweight");
            } else if (bmi > 30)
            {
                Console.WriteLine("You are obese, i strongly recommend visiting a doctor or something");
            }
        }
    }
}
